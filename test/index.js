import Forum from '../src/lib/safira'

import Recording from './recording'
import {sessionVars} from './substitutions'


export default function testMethod(fixture, vars, fn) {
  if (typeof vars === 'function' && !fn) {
    // Called with no vars, so assume none.
    fn = vars
    vars = []
  }

  // Create and initialize the forum interface.
  const safira = new Forum('https://example.com/forum/')
  safira.loggedInAs = 'safira'

  // Load the base-session fixture and set the login cookies manually,
  // all because I don't want to duplicate and reprocess the cookie
  // strings here (which would be simpler, really).
  const loginPath = '/forum/ucp.php?mode=login'
  const recording = new Recording()
  recording.vars = new Map(sessionVars().concat(vars))
  recording.load('base-session')
  const loginRec = Array.find(recording.defs, def => def.method === 'POST' && def.path === loginPath)
  for (let cookie of loginRec.headers['set-cookie']) {
    safira.cookies.setCookie(cookie, `https://example.com${loginPath}`)
  }

  // Replay the test fixture with our interface.
  return Recording.replay(fixture, recording.vars, () => fn(safira))
}
