import {randomBytes} from 'crypto'


export function randHex(nbytes) {
  return randomBytes(nbytes).toString('hex')
}

export function randInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}



export function sessionVars() {
  return [
    ['cookie', `phpbb3_${randInt(2**21, 2**25).toString(35).replace(/0/g, 'z')}`],
    ['login.sid', randHex(16)],
    ['logout.sid', randHex(16)],
    ['sid', randHex(16)],
    ['uid', randInt(0, 2**16)],
  ]
}

export function formVars() {
  return [
    ['creation_time', randInt(0, 2**32)],
    ['form_token', randHex(20)],
  ]
}
