import {assert} from 'chai'

import testMethod from '..'
import {formVars, randInt} from '../substitutions'
import {get as getFixture} from '../fixtures'


const topicView = JSON.parse(getFixture('data', 'topic-view.json').contents)


suite('Topics:', () => {
  test('listing', () => {
    const forumId = randInt(1, 2**16), threadId = randInt(1, 2**16)
    const vars = [
      ['f', forumId],
      ['t', threadId],
      ['start', 0],
    ]

    const topicListing = new Array(topicView.length)
    for (let i = 0; i < topicView.length; i++) {
      topicListing[i] = Object.assign({}, topicView[i])
      delete topicListing[i].message
    }

    return testMethod('topic-view', vars, safira =>
      assert.becomes(safira.listPosts(forumId, threadId, 0), topicListing)
    )
  })
  test('reading', async() => {
    for (let msg of topicView) {
      const forumId = randInt(1, 2**16), threadId = randInt(1, 2**16)
      const vars = [
        ['f', forumId],
        ['t', threadId],
        ['p', msg.id],
        ['start', 0],
      ]

      await testMethod('post-view', vars, safira =>
        assert.becomes(safira.getPost(msg.id), msg)
      )
    }
  })
  suite('posting', () => {
    function reply(options) {
      const forumId = randInt(1, 2**16), threadId = randInt(1, 2**16)
      const vars = formVars().concat([
        ['f', forumId],
        ['t', threadId],
        ['p', randInt(1, 2**16)],
      ])

      return testMethod('topic-reply', vars, safira =>
        safira.sendPost(forumId, threadId,
                        'hello', "Let's avoid percent encoding, ya?", options)
      )
    }

    test('to an existing topic', () => reply())
    test('with unrecognized options', () => reply({oops: true}))
  })
  test('editing', () => {
    const forumId = randInt(1, 2**16), postId = randInt(1, 2**16)
    const vars = formVars().concat([
      ['f', forumId],
      ['p', postId],
      ['t', randInt(1, 2**16)],
      ['lastclick', randInt(1364526000, 1423699200)],
    ])

    return testMethod('post-edit', vars, safira =>
      safira.editPost(forumId, postId,
                      "I died but I'm doing well.",
                      "This message has been edited.  Pray I don't edit it further.")
    )
  })
  test('deleting', () => {
    const forumId = randInt(1, 2**16), postId = randInt(1, 2**16)
    const vars = formVars().concat([
      ['f', forumId],
      ['t', randInt(1, 2**16)],
      ['p', postId],
      ['confirm_key', randInt(2**47, 2**51).toString(36).toUpperCase()],
    ])

    return testMethod('post-delete', vars, safira =>
      safira.deletePost(forumId, postId)
    )
  })
})
