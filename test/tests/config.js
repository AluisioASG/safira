import {assert} from 'chai'

import {loadConfig} from '../../src/lib/config'

import {get as getFixture} from '../fixtures'


function loadConfigFixture(name) {
  process.env.SAFIRA_CONFIG = getFixture('configs', name).path
  return loadConfig()
}

suite('CLI config:', () => {
  test('error on unreadable config file', () => {
    process.env.SAFIRA_CONFIG = __filename + '.notfound'
    assert.throws(() => loadConfig())
  })
  test('error on malformed config file', () => {
    assert.throws(() => loadConfigFixture('not-toml.json'))
  })
  test('error on semantically invalid config file', () => {
    assert.throws(() => loadConfigFixture('invalid.toml'))
    assert.throws(() => loadConfigFixture('no-credentials.toml'))
    assert.throws(() => loadConfigFixture('deprecated.toml'), /favorite_threads/)
    assert.throws(() => loadConfigFixture('deprecated.toml'), /\bforum\b.+ does not match pattern/)
  })
  test('no error on correct config file', () => {
    const result = JSON.parse(getFixture('configs', 'not-toml.json').contents)
    assert.deepEqual(loadConfigFixture('ok.toml'), result)
  })
})
