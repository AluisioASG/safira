import testMethod from '..'
import {formVars} from '../substitutions'


suite('UCP:', () => {
  test('signature change', () => {
    const vars = formVars().concat([
      ['ucp.linkback', 'mode=signature'],
    ])

    return testMethod('signature-change', vars, safira =>
      safira.setSignature('capturing signature change protocol…\n')
    )
  })
})
