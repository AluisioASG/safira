import {assert} from 'chai'

import commands from '../../src/bin/commands'

import {get as getFixture} from '../fixtures'


// Fake the command runner so it doesn't actually run the commands,
// but makes it available to us to inspect instead.
let lastCommand
const commandRunnerPath = require.resolve('../../src/bin/run-command')
require.cache[commandRunnerPath] = {
  id: commandRunnerPath,
  filename: commandRunnerPath,
  loaded: true,
  exports: function runCommand(context, command, args) {
    lastCommand = [command, ...args]
    return Promise.resolve()
  },
}

// Patch our command-line parser to enable debug mode ìn `argparse`.
const cliParserPath = require.resolve('../../src/bin/cli-parser')
const CommandLineParser = require(cliParserPath)
require.cache[cliParserPath].exports = class extends CommandLineParser {
  constructor() {
    super(...arguments)
    this.mainParser.debug = true
    this.commandsParser.debug = true
  }
}

// Fix the path to the config file.
process.env.SAFIRA_CONFIG = getFixture('configs', 'ok.toml').path


// Patch up the process' command line and rerun the CLI entry point
// module.
function run(...args) {
  process.argv.splice(2, process.argv.length, ...args)
  const entryPointPath = require.resolve('../../src/bin')
  delete require.cache[entryPointPath]
  require(entryPointPath)
}


suite('CLI routing:', () => {
  for (let [name, command] of Object.entries(commands)) {
    test(name, () => {
      run(name, ...command.args)
      assert.deepEqual(lastCommand, [command, ...command.args])
      if (command.args.length > 0) {
        assert.throws(() => run(name))
      }
    })
  }
})

test('CLI command parameters', () => {
  const TESTS = [
    ['guarany',     ['f', 't'], ['7', '101043']],
    ['guarany+p21', ['f', 'p'], ['7', '21']],
    ['that_post',   ['p'],      ['123']],
    ['?f=-1&t=-2',  ['t', 'f'], ['-2', '-1']],
  ]
  const THROWS = [
    ['that_post', ['f', 't', 'start']],
    ['guarany',  ['p']],
  ]

  const commandContext = require('../../src/bin/command-context')
  for (let [arg, params, result] of TESTS) {
    assert.deepEqual(commandContext.Parameters(arg, ...params), result)
  }
  for (let [arg, params] of THROWS) {
    assert.throws(() => commandContext.Parameters(arg, ...params))
  }
})
