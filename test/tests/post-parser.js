import path from 'path'

import {assert} from 'chai'

import parsePost from '../../src/bin/post-parser'

import {get as getFixture} from '../fixtures'


suite('Post/message parsing:', () => {
  for (let {path: filename, contents} of getFixture('messages')) {
    // Split BBCode from test options and result through a form feed.
    // If your editor doesn't display them nicely, that's too bad.
    let [bbcode, parsed] = contents.split('\f')
    parsed = JSON.parse(parsed)
    // Let tests specify a custom `posting_options` config.
    let config = {Config: {posting_options: parsed.CONFIG || {}}}
    delete parsed.CONFIG

    test(path.basename(filename), () => {
      // Allow tests to specify they'll error.
      if (parsed.TEST_WILL_THROW) {
        assert.throws(() => parsePost(config, bbcode))
      } else {
        assert.deepEqual(parsePost(config, bbcode), parsed)
      }
    })
  }
})
