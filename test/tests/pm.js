import {assert} from 'chai'

import testMethod from '..'
import {formVars, randInt} from '../substitutions'
import {get as getFixture} from '../fixtures'


const inbox = JSON.parse(getFixture('data', 'pm-inbox.json').contents)
const outbox = JSON.parse(getFixture('data', 'pm-outbox.json').contents)
const sentbox = JSON.parse(getFixture('data', 'pm-sentbox.json').contents)


suite('Private messages:', () => {
  test('listing inbox', async() => {
    // Iterate over every possible combination of unread messages.
    // If this becomes slow or overflows, we've got too many messages.
    for (let i = 0; i < Math.pow(2, inbox.length); i++) {
      // Turn the index into a binary string of length same as the
      // number of messages in the inbox, then make the string into
      // an array of booleans indicating whether a message is unread.
      let bstr = i.toString(2)
      bstr = String.repeat('0', inbox.length - bstr.length) + bstr
      const unread = [for (b of bstr) !!+b]

      // Setup the fixture substitutions and the expected output.
      const vars = new Array(inbox.length), output = new Array(inbox.length)
      for (let i = 0; i < inbox.length; i++) {
        vars[i] = [`pm${i + 1}.unread`, unread[i] ? 'un' : '']
        output[i] = Object.assign({read: !unread[i]}, inbox[i])
        delete output[i].message
      }

      await testMethod('pm-list-inbox', vars, safira =>
        assert.becomes(safira.listReceivedPrivateMessages(), output)
      )
    }
  })
  test('listing sentbox', async() => {
    await testMethod('pm-list-sentbox', safira =>
      assert.becomes(safira.listSentPrivateMessages('sentbox'), sentbox)
    )
  })
  test('listing outbox', async() => {
    await testMethod('pm-list-outbox', safira =>
      assert.becomes(safira.listSentPrivateMessages('outbox'), outbox)
    )
  })
  test('reading', () => {
    const messageData = Array.find(inbox, e => e.id === '18102')
    return testMethod('pm-read', safira =>
      assert.becomes(safira.getPrivateMessage('18102'), messageData)
    )
  })
  test('sending', () => {
    const vars = formVars().concat([
      ['lastclick', randInt(1364526000, 1423699200)]
    ])

    return testMethod('pm-send', vars, safira =>
      safira.sendPrivateMessage('mrob27', 'Re: things', 'I love you[color=#0f52ba]❣[/color]  (Also figuring out what I do when I send these messages) [i]-- safira[/i]\n', {attach_sig: true})
    )
  })
  test('editing', () => {
    const vars = formVars().concat([
      ['lastclick', randInt(1364526000, 1423699200)],
    ])

    return testMethod('pm-edit', vars, safira =>
      safira.editPrivateMessage(247036, "DON'T READ THIS", 'Now: how to edit a PM.  Next: how to vaporize recipients.\n', {})
    )
  })
  test('deleting', () => {
    const vars = formVars().concat([
      ['confirm_key', randInt(2**47, 2**51).toString(36).toUpperCase()],
    ])

    return testMethod('pm-delete', vars, safira =>
      safira.deletePrivateMessage(247036)
    )
  })
})
