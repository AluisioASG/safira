import {assert} from 'chai'

import Forum from '../../src/lib/safira'

import Recording from '../recording'
import {sessionVars} from '../substitutions'


suite('Session management:', () => {
  test('basic login/logout sequence', () => {
    return Recording.replay('base-session', sessionVars(), async() => {
      const safira = new Forum('https://example.com/forum/')
      await safira.login('safira', 'arifas')
      await safira.logout()
    })
  })
  test('error on wrong credentials', () => {
    return Recording.replay('login-failure', sessionVars(), async() => {
      const safira = new Forum('https://example.com/forum/')
      await assert.isRejected(safira.login('safira', 'safira'), /incorrect password/i)
    })
  })
  test('error on relogin as someone else', () => {
    return Recording.replay('base-session', sessionVars(), async() => {
      const safira = new Forum('https://example.com/forum/')
      await safira.login('safira', 'arifas')
      await assert.isRejected(safira.login('AluisioASG', 'myPa55word'), /already logged in/i)
      await safira.logout()
    })
  })
})
