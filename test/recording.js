import nock from 'nock'

import {default as E, ERROR_PROPERTIES as EP} from '../src/lib/quasis/error'

import {get as getFixture} from './fixtures'


export default class Recording {
  constructor() {
    this.defs = []
  }
  
  // Read a recording or page file as text and process substitutions.
  _readFixture(group, name, ext) {
    const fixture = getFixture(group, `${name}.${ext}`)
    return this._processSubstitutions(fixture.contents, fixture.path)
  }

  // Process all variable substitutions ((`{{something}}`)) in a string,
  // throwing if a substitution hasn't been mapped yet.
  _processSubstitutions(text, source) {
    return text.replace(/\{\{([^#]+?)\}\}/g, (_, key) => {
      if (!this.vars.has(key)) {
        throw E`
          ${source} needs a substitution for {{${key}}}
          ${EP} NoSubst ${source}:file ${key}:key
        `
      }
      return this.vars.get(key)
    })
  }

  // If the given string is an include substitution (`{{#something#}}`),
  // read the page file named by it and return its processed text, else
  // return the string unmodified.
  _processInclude(text) {
    return text.replace(/^\{\{#(.+?)#\}\}$/, (_, name) => {
      return this._readFixture('pages', name, 'html')
    })
  }

  // Load a recording file, processing substitutions as they're found.
  load(name) {
    const recording = JSON.parse(this._readFixture('recordings', name, 'json'))
    for (let rec of recording) {
      rec.response = this._processInclude(rec.response)
      rec.body = this._processInclude(rec.body)
    }
    this.defs.push(...recording)
  }
  
  static async replay(fixture, vars, fn) {
    const recording = new Recording()
    recording.vars = new Map(vars)
    recording.load(fixture)

    nock.cleanAll()
    const nocks = nock.define(recording.defs)
    for (let mock of nocks) mock.replyContentLength()
    await fn()
    for (let mock of nocks) mock.done()
  }
}
