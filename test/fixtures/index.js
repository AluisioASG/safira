import fs from 'fs'
import path from 'path'


export function get(group, basename='') {
  const filename = path.join(__dirname, group, basename)
  if (fs.statSync(filename).isDirectory()) {
    return [for (entry of fs.readdirSync(filename)) get(group, entry)]
  } else {
    return {
      path: filename,
      contents: fs.readFileSync(filename, 'utf8'),
    }
  }
}
