<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" media-type="text/x-bbcode" indent="yes" encoding="UTF-8"/>

  <xsl:template match="br">
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>

  <!-- Positioning blocks -->
  <xsl:template match="div[@align='center']">
    <xsl:text>[center]</xsl:text><xsl:apply-templates/><xsl:text>[/center]</xsl:text>
  </xsl:template>
  <xsl:template match="div[@align='right']">
    <xsl:text>[right]</xsl:text><xsl:apply-templates/><xsl:text>[/right]</xsl:text>
  </xsl:template>

  <!-- Links -->
  <xsl:template match="img">
    <xsl:text>[img]</xsl:text><xsl:value-of select="@src"/><xsl:text>[/img]</xsl:text>
  </xsl:template>
  <xsl:template match="a[@class='postlink']">
    <xsl:variable name="arg" select="@href"/>
    <xsl:choose>
      <xsl:when test="@href = .">
        <xsl:text>[url]</xsl:text><xsl:value-of select="$arg"/><xsl:text>[/url]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[url=</xsl:text><xsl:value-of select="$arg"/><xsl:text>]</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>[/url]</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Parameter-less inline text formatting -->
  <xsl:template match="span[contains(@style, 'font-weight: bold')]">
    <xsl:text>[b]</xsl:text><xsl:apply-templates/><xsl:text>[/b]</xsl:text>
  </xsl:template>
  <xsl:template match="span[contains(@style, 'font-style: italic')]">
    <xsl:text>[i]</xsl:text><xsl:apply-templates/><xsl:text>[/i]</xsl:text>
  </xsl:template>
  <xsl:template match="span[contains(@style, 'text-decoration: underline')]">
    <xsl:text>[u]</xsl:text><xsl:apply-templates/><xsl:text>[/u]</xsl:text>
  </xsl:template>
  <xsl:template match="strike">
    <xsl:text>[s]</xsl:text><xsl:apply-templates/><xsl:text>[/s]</xsl:text>
  </xsl:template>
  <xsl:template match="sup">
    <xsl:text>[sup]</xsl:text><xsl:apply-templates/><xsl:text>[/sup]</xsl:text>
  </xsl:template>
  <xsl:template match="sub">
    <xsl:text>[sub]</xsl:text><xsl:apply-templates/><xsl:text>[/sub]</xsl:text>
  </xsl:template>

  <!-- Parametrized inline text formatting -->
  <xsl:template match="span[contains(@style, 'font-size:')]">
    <xsl:variable name="arg" select="substring-before(substring-after(@style, 'font-size: '), '%;')"/>
    <xsl:text>[size=</xsl:text><xsl:value-of select="$arg"/><xsl:text>]</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>[/size]</xsl:text>
  </xsl:template>
  <xsl:template match="span[contains(@style, 'color:')]">
    <xsl:variable name="arg" select="substring-after(@style, 'color: ')"/>
    <xsl:text>[color=</xsl:text><xsl:value-of select="$arg"/><xsl:text>]</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>[/color]</xsl:text>
  </xsl:template>

  <!-- Lists -->
  <xsl:template match="ul">
    <xsl:text>[list]</xsl:text><xsl:apply-templates/><xsl:text>[/list]</xsl:text>
  </xsl:template>
  <xsl:template match="ol">
    <xsl:text>[list=1]</xsl:text><xsl:apply-templates/><xsl:text>[/list]</xsl:text>
  </xsl:template>
  <xsl:template match="li">
    <xsl:text>[*]</xsl:text><xsl:apply-templates/>
  </xsl:template>

  <!-- Containers -->
  <xsl:template match="blockquote[contains(@class, 'uncited')]">
    <xsl:text>[quote]</xsl:text><xsl:apply-templates select="./div"/><xsl:text>[/quote]</xsl:text>
  </xsl:template>
  <xsl:template match="blockquote[not(contains(@class, 'uncited'))]">
    <xsl:variable name="arg" select="substring-before(./div/cite[position()=1], ' wrote')"/>
    <xsl:text>[quote="</xsl:text><xsl:value-of select="$arg"/><xsl:text>"]</xsl:text>
    <xsl:apply-templates select="./div/node()[position()!=1]"/>
    <xsl:text>[/quote]</xsl:text>
  </xsl:template>
  <xsl:template match="div[./div[@class='quotetitle']/b[text()='Spoiler:']]">
    <xsl:text>[spoiler]</xsl:text>
    <xsl:apply-templates select="./div[@class='quotecontent']/div"/>
    <xsl:text>[/spoiler]</xsl:text>
  </xsl:template>
  <xsl:template match="dl[@class='codebox']">
    <xsl:text>[code]</xsl:text><xsl:apply-templates select=".//code"/><xsl:text>[/code]</xsl:text>
  </xsl:template>

</xsl:stylesheet>
