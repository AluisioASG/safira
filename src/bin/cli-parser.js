import {ArgumentParser} from 'argparse'


// The key into the parsed arguments where we store the command object
// that was selected.  This is one of the cases where we'd normally use
// a Symbol but the underlying library doesn't support it, because it
// uses `Object.keys` to iterate over the commands' defaults.
const COMMAND = `${module.filename}::COMMAND`


// A command-line parser with support for safira's command object
// format, based upon the `argparse` package.
export default class CommandLineParser {
  // Create a skeleton for the command-line parser given the package's
  // metadata.
  constructor(pkginfo) {
    this.mainParser = new ArgumentParser({description: pkginfo.description})
    this.mainParser.addArgument(['-V', '--version'], {
      help: 'Output the version number and exit.',
      action: 'version',
      version: `${pkginfo.name} ${pkginfo.version}`,
    })
    this.commandsParser = this.mainParser.addSubparsers({
      title: 'Subcommands',
      description: 'Run with "<subcommand> --help" for details.',
    })
  }

  // Add a command subparser for the given name and command object.
  addCommand(name, command) {
    // Set up the subparser for the command.
    let commandParser = this.commandsParser.addParser(name, {
      description: command.description,
    })
    for (let arg of command.args) {
      commandParser.addArgument([arg], {})
    }

    // Make the command object available to us once we've parsed which
    // command we want to run.
    commandParser.setDefaults({[COMMAND]: command})

    // Allow chaining.
    return this
  }

  // Parse either the given arguments or the process' command line and
  // return the command object selected and its arguments.
  parse(args) {
    let parsed = this.mainParser.parseArgs(args), command = parsed[COMMAND]
    // Remap the arguments in the order the command module expects them.
    let commandArgs = [for (name of command.args) parsed[name]]
    return {result: parsed, command: command, args: commandArgs}
  }
}
