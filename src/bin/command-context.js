import {spawn} from 'child_process'

import {fromPackageRoot} from '../lib/package-root'
import {loadConfig} from '../lib/config'
import getParams from './params-parser'


function collectStream(stream) {
  const output = []
  return new Promise((resolve, reject) => {
    stream.on('data', chunk => output.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(output).toString()))
  })
}


// Command context with logging to `stderr` and IO on the other two
// standard streams, and config from a file.
export default {
  Logger: {
    // Logs an action that's about to happen.
    log(msg) {
      process.stderr.write(`${msg}... `)
    },
    // Logs the successful completion of the previous action; when
    // used in a promise chain, the result of the chain so far is
    // passed through.
    ok(arg) {
      process.stderr.write(`ok.\n`)
      return arg
    },
    // Logs an error and rethrows it.
    err(err) {
      process.stderr.write(`${err.stack || err}\n`)
      // Prevent `./index` from reporting this error again.
      throw null
    },
  },

  // I/O methods.
  IO: {
    // Name of the transformer to be run by `#transform`.
    get transformer() { return this._transformerName },
    set transformer(value) { this._transformerName = (value === 'none' || !value) ? null : value },
    // Reads the standard input stream as a string.
    input() {
      return collectStream(process.stdin)
    },
    // Writes a string to the standard output stream.
    output(text) {
      return new Promise((resolve, reject) =>
        process.stdout.write(`${text}`, err => err ? reject(err) : resolve())
      )
    },
    // Transforms a chunk of XML.
    transform(xml) {
      if (!this.transformer) return xml
      const xslPath = fromPackageRoot(`post-xslt/${this.transformer}.xsl`)
      return new Promise((resolve, reject) => {
        const child = spawn('xsltproc', [xslPath, '-'], {stdio: ['pipe', 'pipe', process.stderr]})
        const collect = collectStream(child.stdout)
        child.on('exit', (code, signal) => code === 0 ? resolve(collect) : reject(code || signal))
        child.stdin.end(xml)
      })
    },
  },

  // User config.
  Config: loadConfig(),

  // Forum session, set by `./run-command::initSession`.
  Forum: null,

  // Parse the command argument and extract the requested parameters.
  // Configured aliases are checked for a match before parsing.
  Parameters(cmdarg, ...params) {
    if (this.Config.aliases) {
      // Aliases are available.  Re-parse the command argument and see
      // if it matches a known alias.
      const [alias, extras] = cmdarg.split('+')
      const def = this.Config.aliases[alias]
      if (def) cmdarg = def + (extras || '')
    }
    return getParams(cmdarg, ...params)
  },
}
