import {parse as parseUrl} from 'url'

import {default as E, ERROR_PROPERTIES as EP} from '../lib/quasis/error'


function getParamsFromUrl(url, ...params) {
  const {query} = parseUrl(url, true)
  const result = []
  for (let param of params) {
    if (param in query) {
      result.push(query[param])
    } else {
      throw E`
        Missing parameter "${param}" in URL "${url}"
        ${EP} ArgumentError
      `
    }
  }
  return result
}

function isSpec(spec) {
  return /^(?:\D\d+)+$/.test(spec)
}

function getParamsFromSpec(spec, ...params) {
  const SPEC_PARAM_REGEX = /(\D)(\d+)/g, args = []
  let match
  while ((match = SPEC_PARAM_REGEX.exec(spec))) {
    args[match[1]] = match[2]
  }

  const result = []
  for (let param of params) {
    param = param[0]
    if (param in args) {
      result.push(args[param])
    } else {
      throw E`
        Missing parameter "${param}" in spec "${spec}"
        ${EP} ArgumentError
      `
    }
  }
  return result
}


export default function getParams(str, ...params) {
  if (params.length === 1 && !Number.isNaN(+str)) {
    // If there's a single parameter, accept it alone as well.
    return [str]
  } else if (isSpec(str)) {
    // Parse it as a sequence of undelimited key-value pairs.
    return getParamsFromSpec(str, ...params)
  } else {
    // Parse the string as a possible URL and extract the parameters
    // from the query string.
    return getParamsFromUrl(str, ...params)
  }
}
