#!/usr/bin/env node

import {MESSAGE_TRANSFORMS_SCHEMA} from '../lib/config'
import {loadFromPackageRoot} from '../lib/package-root'
import CommandLineParser from './cli-parser'
import commands from './commands'
import commandContext from './command-context'
import runCommand from './run-command'


// Parse the command line.
let parser = new CommandLineParser(loadFromPackageRoot('package.json'))
parser.mainParser.addArgument(['-f', '--output-format'], {
  help: 'Select the output format for posts (requires xsltproc).',
  choices: MESSAGE_TRANSFORMS_SCHEMA.enum,
})
for (let name of Object.keys(commands)) {
  parser.addCommand(name, commands[name])
}
let {result: gargs, command, args} = parser.parse()

// Update the command context with the post output transform.
commandContext.IO.transformer =
  gargs.output_format ||
  (commandContext.Config.output && commandContext.Config.output.transform) ||
  null

// Now run the desired command.
runCommand(commandContext, command, args)
.catch(err => { if (err) console.error(err.stack || err) })
