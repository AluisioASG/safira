import Forum from '../lib/safira'


// Log in the forum.
async function initSession(ctx) {
  ctx.Logger.log(`Logging in as ${ctx.Config.credentials.username}`)
  ctx.Forum = new Forum(ctx.Config.forum)
  let {username, password} = ctx.Config.credentials
  await ctx.Forum.login(username, password).then(ctx.Logger.ok, ctx.Logger.err)
}

// Log out of the forum.
async function endSession(ctx) {
  // Check if we did log in.
  if (ctx.Forum.loggedInAs === null) return
  ctx.Logger.log('Logging out')
  await ctx.Forum.logout().then(ctx.Logger.ok, ctx.Logger.err)
}


export default async function runCommand(context, command, args) {
  // Initialize the forum session, run the given command and terminate
  // the session regardless of errors.
  await initSession(context)
  try {
    await command.run.apply(context, args)
  } finally {
    await endSession(context)
  }
}
