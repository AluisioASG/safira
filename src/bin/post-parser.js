import {validateFromFile as validateConfig, POSTING_OPTIONS_SCHEMA} from '../lib/config'


export default function(commandContext, input) {
  // Remove all empty and whitespace-only strings from the beginning
  // of a string array.
  function skipEmptyLines(lines) {
    let skip = 0
    for (let line of lines) {
      if (/^\s*$/.test(line)) {
        skip++
      } else {
        break
      }
    }
    lines.splice(0, skip)
  }

  let contents = input.split(/\r?\n/)
  // Try to read a JSON options object from the first line.
  let options
  try {
    skipEmptyLines(contents)
    options = JSON.parse(contents[0])
    contents.shift()
  } catch (_) {
    options = {}
  }
  validateConfig('<post options>', options, POSTING_OPTIONS_SCHEMA)

  // Use the first non-options non-empty line as the subject.
  skipEmptyLines(contents)
  let subject = contents.shift()

  // Skip more empty lines and assume the remaining lines are the body.
  skipEmptyLines(contents)
  let message = contents.join('\n')

  // Get the user's config and apply the default posting options.
  let defaultOptions = commandContext.Config.posting_options || {}
  for (let opt of Object.keys(defaultOptions)) {
    if (!(opt in options)) {
      options[opt] = defaultOptions[opt]
    }
  }

  return {
    options,
    subject,
    message,
  }
}
