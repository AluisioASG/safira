import parsePost from '../../post-parser'


export default {
  description: 'Change your signature',
  run: async function() {
    this.Logger.log('Reading new signature')
    let post = await this.IO.input().then(text => parsePost(this, text))
    .then(this.Logger.ok, this.Logger.err)

    this.Logger.log(`Sending signature change request`)
    let {message: text, options} = post
    await this.Forum.setSignature(text, options)
    .then(this.Logger.ok, this.Logger.err)
  },
}
