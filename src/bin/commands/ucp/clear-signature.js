export default {
  description: 'Clear your signature',
  run: async function() {
    this.Logger.log(`Sending signature change request`)
    await this.Forum.setSignature('', {})
    .then(this.Logger.ok, this.Logger.err)
  },
}
