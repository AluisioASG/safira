// Fill any unset properties of the command object.
function normalizeCommand(name, command) {
  return Object.assign({
    name: name,
    args: [],
    description: '',
    run: () => {},
  }, command)
}
function requireCommand(name) {
  return normalizeCommand(name, require(`./${name}`))
}


export default {
  'noop':            requireCommand('noop'),
  'set-signature':   requireCommand('ucp/set-signature'),
  'clear-signature': requireCommand('ucp/clear-signature'),
  'get-pm':          requireCommand('pm/get'),
  'edit-pm':         requireCommand('pm/edit'),
  'delete-pm':       requireCommand('pm/delete'),
  'new-pm':          requireCommand('pm/new'),
  'get-pm-inbox':    requireCommand('pm/get-inbox'),
  'get-pm-outbox':   requireCommand('pm/get-outbox'),
  'get-pm-sentbox':  requireCommand('pm/get-sentbox'),
  'get-post':        requireCommand('post/get'),
  'edit-post':       requireCommand('post/edit'),
  'delete-post':     requireCommand('post/delete'),
  'new-post':        requireCommand('post/new'),
  'get-posts':       requireCommand('post/get-topic'),
}
