export default {
  args: ['target'],
  description: 'Delete a forum post (if not replied to)',
  run: async function(arg) {
    const [forumId, postId] = this.Parameters(arg, 'f', 'p')

    this.Logger.log('Deleting post')
    await this.Forum.deletePost(forumId, postId)
    .then(this.Logger.ok, this.Logger.err)
  }
}
