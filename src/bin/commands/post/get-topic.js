import columnify from 'columnify'


export default {
  args: ['target'],
  description: 'Get the posts in a given forum thread page',
  run: async function(arg) {
    const [forumId, threadId, start] = this.Parameters(arg, 'f', 't', 'start')

    this.Logger.log('Fetching post list')
    await this.Forum.listPosts(forumId, threadId, start)
    .then(this.Logger.ok, this.Logger.err)
    .then(messages => messages.map((msg, i) => Object.assign({order: i + 1}, msg)))
    .then(messages => this.IO.output(columnify(messages) + '\n'))
  }
}
