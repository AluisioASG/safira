import parsePost from '../../post-parser'


export default {
  args: ['target'],
  description: 'Edit a post',
  run: async function(arg) {
    const [forumId, postId] = this.Parameters(arg, 'f', 'p')

    this.Logger.log('Reading message')
    let {subject, message, options} = await this.IO.input().then(text => parsePost(this, text))
    .then(this.Logger.ok, this.Logger.err)

    this.Logger.log('Sending edit')
    await this.Forum.editPost(forumId, postId, subject, message, options)
    .then(this.Logger.ok, this.Logger.err)
  },
}
