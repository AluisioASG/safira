import parsePost from '../../post-parser'


export default {
  args: ['destination'],
  description: 'Reply to a forum topic',
  run: async function(arg) {
    const [forumId, threadId] = this.Parameters(arg, 'f', 't')

    this.Logger.log('Reading message')
    let {subject, message, options} = await this.IO.input().then(text => parsePost(this, text))
    .then(this.Logger.ok, this.Logger.err)

    this.Logger.log('Posting')
    await this.Forum.sendPost(forumId, threadId, subject, message, options)
    .then(this.Logger.ok, this.Logger.err)
  },
}
