export default {
  args: ['id'],
  description: 'Read a post',
  run: async function(arg) {
    const [postId] = this.Parameters(arg, 'p')

    this.Logger.log(`Fetching post ${postId}`)
    let {author, date, subject, message} = await this.Forum.getPost(postId)
    .then(this.Logger.ok, this.Logger.err)
    message = await this.IO.transform(message)
    this.IO.output(`From ${author} at ${date}: ${subject}\n${message}\n`)
  },
}
