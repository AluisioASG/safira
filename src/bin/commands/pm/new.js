import parsePost from '../../post-parser'


export default {
  args: ['recipient'],
  description: 'Send a private message',
  run: async function(recipient) {
    this.Logger.log('Reading message')
    let post = await this.IO.input().then(text => parsePost(this, text))
    .then(this.Logger.ok, this.Logger.err)

    this.Logger.log(`Sending PM to ${recipient}`)
    let {subject, message, options} = post
    await this.Forum.sendPrivateMessage(recipient, subject, message, options)
    .then(this.Logger.ok, this.Logger.err)
  },
}
