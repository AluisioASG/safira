export default {
  args: ['id'],
  description: 'Deletes a private message if not yet read',
  run: async function(id) {
    this.Logger.log(`Sendind deletion request`)
    await this.Forum.deletePrivateMessage(id)
    .then(this.Logger.ok, this.Logger.err)
  },
}
