import columnify from 'columnify'


export default {
  description: 'List private messages, optionally filtering by status (read, unread, all)',
  args: ['status'],
  run: async function(status) {
    this.Logger.log('Fetching list of private messages in the inbox')
    let messages = await this.Forum.listReceivedPrivateMessages()
    .then(this.Logger.ok, this.Logger.err)
    
    switch (status) {
      case 'all':
        break
      case 'read':
        messages = messages.filter(msg => msg.read)
        break
      case 'unread':
        messages = messages.filter(msg => !msg.read)
        break
      default:
        throw `Invalid message status filter: ${status}`
    }
    
    if (messages.length === 0) {
      this.IO.output('No messages selected\n')
    } else {
      this.IO.output(columnify(messages) + '\n')
    }
  },
}
