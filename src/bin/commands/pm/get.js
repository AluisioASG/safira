export default {
  args: ['id'],
  description: 'Read a private message',
  run: async function(id) {
    this.Logger.log(`Fetching message ${id}`)
    let {sender, date, subject, message} = await this.Forum.getPrivateMessage(id)
    .then(this.Logger.ok, this.Logger.err)
    message = await this.IO.transform(message)
    this.IO.output(`From ${sender} at ${date}: ${subject}\n${message}\n`)
  },
}
