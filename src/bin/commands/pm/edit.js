import parsePost from '../../post-parser'


export default {
  args: ['id'],
  description: 'Edits a private message',
  run: async function(id) {
    this.Logger.log('Reading message revision')
    let post = await this.IO.input().then(text => parsePost(this, text))
    .then(this.Logger.ok, this.Logger.err)

    this.Logger.log(`Editing PM #${id}`)
    let {subject, message, options} = post
    await this.Forum.editPrivateMessage(id, subject, message, options)
    .then(this.Logger.ok, this.Logger.err)
  },
}
