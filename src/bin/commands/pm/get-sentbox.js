import columnify from 'columnify'


export default {
  description: 'List sent private messages read by the recipient',
  run: async function() {
    this.Logger.log('Fetching list of private messages')
    let messages = await this.Forum.listSentPrivateMessages('sentbox')
    .then(this.Logger.ok, this.Logger.err)

    if (messages.length === 0) {
      this.IO.output('No messages found\n')
    } else {
      this.IO.output(columnify(messages) + '\n')
    }
  },
}
