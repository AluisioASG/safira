export default {
  description: 'Do nothing; test login and logout',
  run: function() {
    this.Logger.log('Doing nothing')
    this.Logger.ok()
  },
}
