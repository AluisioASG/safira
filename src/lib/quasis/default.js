// Order the literals and substitutions passed to a template tag as
// they were originally written, so for e.g. `a${1}b${2}` you'd get
// `['a', 1, 'b', 2, '']`.
export function orderTemplate(literals, substitutions) {
  // The resulting array must have length 1 more than expected so we
  // can do a performance trick below.  `ri` is the index of the next
  // free slot in this array.
  let result = new Array(literals.length + substitutions.length + 1), ri = 0
  // We know that
  // - a substitution is always surrounded by literals (even empty), so
  // - there is exactly one more literal than are substitutions, and
  // - we should begin and end with a literal
  // so to put the arrays in order we intercalate them.  We can use a
  // single loop for this by considering they have the same length and
  // then discarding the last substitution.
  for (let i = 0; i < literals.length; i++) {
    result[ri++] = literals[i]
    result[ri++] = substitutions[i]
  }
  result.length--
  return result
}

// The default template literal processing.
export default function defaultTag(literals, ...substitutions) {
  return orderTemplate(literals, substitutions).join('')
}
