import {defineClassProperty} from '../class-mixin'
import {orderTemplate} from './default'


// Symbol used as a substitution in an error template string to
// delimit the error message and separate it from property assignments.
export const ERROR_PROPERTIES = Symbol('message-properties delimiter')

// Tag function for template strings which creates an error object out
// of the template.  Besides specifying the error message, additional
// properties can be assigned to the error object by interpolating the
// `ERROR_PROPERTIES` symbol after the message, optionally setting the
// error class name in the following string literal, and then appending
// `${value}:prop` pairs after that.
export default function makeError(literals, ...substitutions) {
  // Create the resulting error object and add a method to recapture
  // the stack trace.
  let err = defineClassProperty(new Error(), 'restack', function(caller) {
    Error.captureStackTrace(this, caller)
    return this
  })

  let input = orderTemplate(literals, substitutions)
  let beginProps = input.indexOf(ERROR_PROPERTIES)
  // If the message-properties delimiter is present in the input,
  // trim the input array until the delimiter and process the property
  // assignments after it.
  if (beginProps !== -1) {
    let props = input.splice(beginProps, input.length)
    // Skip the first two elements of the properties array, which are
    // the delimiter symbol and the literal (if any) after it.
    for (let i = 2; i < props.length; i += 2) {
      err[props[i+1].trim().slice(1)] = props[i]
    }
    // If the post-symbol literal is non-empty, use it as the
    // error name.
    let maybeName = props[1].trim()
    if (maybeName) defineClassProperty(err, 'name', maybeName)
  }
  // Set the error message from whatever remained in the input array.
  err.message = input.join('').trim()

  // Now set the stack trace correctly and return the created error.
  return err.restack(makeError)
}
