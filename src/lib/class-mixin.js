// Define a property in a object with the same descriptor as if it
// were defined in a class body.
export function defineClassProperty(object, key, value) {
  return Object.defineProperty(object, key, {
    value: value,
    enumerable: typeof value !== 'function',
    configurable: true,
    writable: true,
  })
}

// Extends a constructor's prototype with the enumerable own properties
// in the succeeding object arguments.  Methods are set as unenumerable
// as if they had been defined in a class body.
export default function extendClass(constructor, ...sources) {
  for (let source of sources) {
    for (let key of Reflect.ownKeys(Object(source))) {
      defineClassProperty(constructor.prototype, key, source[key])
    }
  }
  return constructor
}
