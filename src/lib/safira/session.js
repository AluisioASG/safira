import url from 'url'

import {defineClassProperty} from '../class-mixin'
import defaultTLT from '../quasis/default'
import {default as E, ERROR_PROPERTIES as EP} from '../quasis/error'
import {default as http, cookieJar} from '../request'
import delay from '../delay-promise'
import * as M from './util/messages'
import getMessage from './util/error-message'


// Represents a URL fragment relative to a forum base.  Returned by
// `ForumSession#rel` to shortcut requests.
class ForumUrl {
  constructor(session, path) {
    this.session = session
    this.path = path
  }

  request(method, ...args) {
    return this.session.request(method, this.path, ...args)
  }

  toString() {
    return this.session.base + this.path
  }
}
// Add method shorthands to the `ForumUrl` prototype.  Based on how
// `request` does it, though ES6ified.
for (let verb of ['get', 'head', 'post', 'put', 'patch', 'del']) {
  const method = verb === 'del' ? 'DELETE' : verb.toUpperCase()
  defineClassProperty(ForumUrl.prototype, verb, function(...args) {
    return this.request(method, ...args)
  })
}


export default class ForumSession {
  constructor(base) {
    this.base = base
    // The user we're logged in as at the moment, or `null` if
    // logged out.
    this.loggedInAs = null
    // azule's, er, our cookie jar.
    this.cookies = cookieJar()
  }

  // Issues a request to the forum and checks if it's successful at
  // the HTTP layer.  If a payload is passed, it is sent as form data.
  //
  // The caller can also specify if an information message is expected
  // and handleable; if not, an error will be thrown if one is found.
  // Handling messages is only done by default on `GET` requests.
  async request(method, path, payload, handleMessages) {
    // Once babel/babel#1064 is fixed, use a default parameter value.
    if (handleMessages == null) handleMessages = method === 'GET'
    // In case of payload, wait a second.
    if (payload) await delay(1000)

    let requestUrl = url.parse(url.resolve(this.base, path))
    let response = await http({
      method: method,
      url: requestUrl,
      form: payload,
      jar: this.cookies,
      followRedirect: false,
    })
    let responseUrl = response.url

    // If we're logged in, we don't expect the response to contain
    // a login form.
    if (this.loggedInAs !== null && /\bid=(["']?)login\1\B/.test(response.body)) {
      throw E`
        Found login form at ${responseUrl.href}
        ${EP} SessionError ${requestUrl}:originalUrl ${responseUrl}:url ${response}:response
      `
    }

    // If we find a message and the caller won't handle one, let's deal
    // with it ourselves.
    if (handleMessages) {
      let message = getMessage(response.body)
      if (message !== null) throw E`
        Unexpected message at ${response.url.href}: "${message}"
        ${EP} OperationError ${requestUrl}:originalUrl ${responseUrl}:url ${response}:response
              ${message}:information
      `
    }

    return response
  }

  // Build an absolute URL for the path given as a template literal
  // relative to the forum's base URL.
  rel(literals, ...substitutions) {
    const path = defaultTLT(literals, ...substitutions.map(encodeURIComponent))
    return new ForumUrl(this, path)
  }

  confirmActionSuccess(response, ...expectedMessages) {
    // phpBB 3.1 doesn't have interstitial pages with messages;
    // success is sinalized with a redirect.
    if (response.statusCode === 302) return
    // If we didn't get redirected, look for a message panel containing
    // one of the expected messages.
    let actualMessage = getMessage(response.body)
    if (Array.includes(expectedMessages, actualMessage)) return

    // Looks like it didn't work, after all.
    // `JSON.stringify` give us quoted strings and `null` for free.
    throw E`
      Unexpected message at ${response.url.href}: ${JSON.stringify(actualMessage)}
      ${EP} ConfirmationFailure ${response.url}:url ${response}:response
            ${actualMessage}:information ${expectedMessages}:expectedMessages
    `
  }

  // Logs in to the forum as the given user.
  async login(username, password) {
    const formUrl = this.rel`ucp.php?mode=login`

    // Update the session's ID value from the cookie jar.
    let updateSID = () => {
      let cookies = this.cookies.getCookies(String(formUrl))
      let sidCookies = cookies.filter(ck => String.endsWith(ck.key, '_sid'))
      if (sidCookies.length !== 1) {
        throw E`${sidCookies.length} SID cookies found; expected just one`
      }
      this.sid = sidCookies[0].value
    }

    // Make sure we're not logged in already as somebody else.
    if (this.loggedInAs !== null && this.loggedInAs !== username) {
      throw E`
        Attempted to log in as ${username} but already logged in as ${this.loggedInAs}
        ${EP} ActiveReloginAttempt ${this.loggedInAs}:currentLogin ${username}:attemptedLogin
      `
    }

    // Request the login page so we can get the SID from the cookies.
    await formUrl.get()
    updateSID()
    // Now proceed to actually log in the user and check if we got
    // the expected response.
    let loginResponse = await formUrl.post({
      username,
      password,
      viewonline: 'on',
      sid: this.sid,
      login: 'Login',
    })
    this.confirmActionSuccess(loginResponse, M.LOGIN_REDIRECT)
    updateSID()  // Yup, we need it again.
    this.loggedInAs = username
  }

  // Logs out from the forum.
  async logout() {
    // Log out of the current session and confirm the action succeeded.
    let logoutResponse = await this.rel`ucp.php?mode=logout&sid=${this.sid}`.get(null, false)
    this.confirmActionSuccess(logoutResponse, M.LOGOUT_REDIRECT)
    this.loggedInAs = null
  }
}
