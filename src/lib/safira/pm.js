import cheerio from 'cheerio'

import E from '../quasis/error'
import * as M from './util/messages'
import {initBBcodeFormResponse} from './util/form-response'


// Used to locate the sender/recipient's username in the PM listings.
const PROFILE_PATH_PREFIX = './memberlist.php?mode=viewprofile'


export default {
  // Sends a private message to the given user.
  async sendPrivateMessage(username, subject, message, options={}) {
    const formRequestUrl = this.rel`ucp.php?i=pm&mode=compose&action=post&add_to=Add&username_list=${username}`
    const formResponseUrl = this.rel`ucp.php?i=pm&mode=compose&action=post`

    let {body: formPage} = await formRequestUrl.get()
    let {$: $form, form: formData} = initBBcodeFormResponse(formPage, options)
    Object.assign(formData, {
      [$form('[value=to]').attr('name')]: 'to',
      icon: options.icon || 0,
      subject: subject,
      message: message,
    })
    let postResponse = await formResponseUrl.post(formData)
    this.confirmActionSuccess(postResponse, M.MESSAGE_STORED)
  },

  // Edits a yet unread private message.
  async editPrivateMessage(id, subject, message, options={}) {
    const formUrl = this.rel`ucp.php?i=pm&mode=compose&action=edit&p=${id}`

    let {body: formPage} = await formUrl.get()
    let {$: $form, form: formData} = initBBcodeFormResponse(formPage, options)
    Object.assign(formData, {
      [$form('[name^="address_list[u]"]').attr('name')]: 'to',
      icon: options.icon || 0,
      subject: subject,
      message: message,
      status_switch: $form('[name=status_switch]').val(),
    })
    let postResponse = await formUrl.post(formData)
    this.confirmActionSuccess(postResponse, M.MESSAGE_STORED)
  },

  // Deletes an unread private message.
  async deletePrivateMessage(messageId) {
    let {body: confirmPage} = await this.rel`ucp.php?i=pm&mode=compose&action=delete&p=${messageId}`.get()
    let $ = cheerio.load(confirmPage)

    let destPath = $('#confirm').attr('action')
    let response = await this.request('POST', destPath, {
      confirm_uid: $('input[name=confirm_uid]').val(),
      sess: $('input[name=sess]').val(),
      sid: $('input[name=sid]').val(),
      confirm: 'Yes',
    })
    this.confirmActionSuccess(response, M.MESSAGE_DELETED)
  },

  // Fetches a private message.
  async getPrivateMessage(id) {
    // Get the specified message.
    let {body: msgBody} = await this.rel`ucp.php?i=pm&mode=view&p=${id}`.get()
    let $ = cheerio.load(msgBody)
    // Extract text fields.
    let subject = $('.first').text()
    let message = $.html('.content', {xmlMode: true, decodeEntities: false})
    // Extract metadata.
    let [date, sender] =
      // Get the message's metadata block, remove whitespace at the ends
      // and tab characters, split the block in lines, and remove each
      // line's header.
      $('.author').text().trim().replace(/\t/g, '')
      .split('\n').map(s => s.replace(/^\w+:\s*/, ''))

    return {
      id,
      subject,
      sender,
      date,
      message,
    }
  },

  // Gets a list of private messages in the user's inbox.
  async listReceivedPrivateMessages(start=0) {
    const UNREAD_MESSAGE_BG = 'url(./styles/prosilver/imageset/topic_unread.gif)'

    // Get the user's inbox.
    let {body: inbox} = await this.rel`ucp.php?i=pm&folder=inbox&start=${start}`.get()
    let $ = cheerio.load(inbox)
    // Discover each entry's data and turn these details into an object.
    return $('.icon').map(function() {
      let $entry = $(this)
      let subject = $entry.find('.topictitle').text()
      let id = $entry.find('[name="marked_msg_id[]"]').val()
      let sender = $entry.find(`a[href^="${PROFILE_PATH_PREFIX}"]`).text()
      let date = /»\s*(.+?)\s*$/.exec($entry.text())[1]
      let read = $entry.css('background-image') !== UNREAD_MESSAGE_BG

      return {
        id,
        subject,
        sender,
        date,
        read,
      }
    })
    // Return an array of the objects we built.
    .get()
  },

  // Gets a list of private messages sent by the logged-in user.
  // Folder must be `outbox` for the list of messages not yet read by
  // the recipient, and `sentbox` for those who've already been read.
  async listSentPrivateMessages(folder, start=0) {
    if (!/^(?:out|sent)box$/.test(folder)) throw E`Invalid folder ${folder}`
    // Get the user's messagebox.
    let {body: mailbox} = await this.rel`ucp.php?i=pm&folder=${folder}&start=${start}`.get()
    let $ = cheerio.load(mailbox)
    // Discover each entry's data and turn these details into an object.
    return $('.icon').map(function() {
      let $entry = $(this)
      let subject = $entry.find('.topictitle').text()
      let id = $entry.find('[name="marked_msg_id[]"]').val()
      let recipient = $entry.find(`a[href^="${PROFILE_PATH_PREFIX}"]`).text()
      let date = $entry.find('.info').text().replace(/^Sent: /, '')

      return {
        id,
        subject,
        recipient,
        date,
      }
    })
    // Return an array of the objects we built.
    .get()
  },
}
