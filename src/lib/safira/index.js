import extendClass from '../class-mixin'
import ForumSession from './session'


export default extendClass(class Forum extends ForumSession {},
                           require('./pm'),
                           require('./topic'),
                           require('./ucp'))
