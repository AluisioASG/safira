import cheerio from 'cheerio'

import * as M from './util/messages'
import {initBBcodeFormResponse} from './util/form-response'


export default {
  // Posts a reply to an existing topic.
  async sendPost(forumId, threadId, subject, message, options) {
    let formUrl = this.rel`posting.php?mode=reply&f=${forumId}&t=${threadId}`

    let {body: formPage} = await formUrl.get()
    let {form: formData} = initBBcodeFormResponse(formPage, options)
    Object.assign(formData, {subject, message})
    let postResponse = await formUrl.post(formData)
    this.confirmActionSuccess(postResponse, M.POST_STORED, M.POST_STORED_MOD)
  },

  // Edits an existing post.
  async editPost(forumId, postId, subject, message, options) {
    let formUrl = this.rel`posting.php?mode=edit&f=${forumId}&p=${postId}`

    let {body: formPage} = await formUrl.get()
    let {$: $form, form: formData} = initBBcodeFormResponse(formPage, options)
    Object.assign(formData, {
      subject,
      message,
      edit_post_subject_checksum: $form('[name=edit_post_subject_checksum]').val(),
      edit_post_message_checksum: $form('[name=edit_post_message_checksum]').val(),
    })
    let postResponse = await formUrl.post(formData)
    this.confirmActionSuccess(postResponse, M.POST_EDITED, M.POST_EDITED_MOD)
  },

  // Deletes an user-owned post.
  async deletePost(forumId, postId) {
    let {body: confirmPage} = await this.rel`posting.php?mode=delete&f=${forumId}&p=${postId}`.get()
    let $ = cheerio.load(confirmPage)

    let destPath = $('#confirm').attr('action')
    let response = await this.request('POST', destPath, {
      confirm_uid: $('input[name=confirm_uid]').val(),
      sess: $('input[name=sess]').val(),
      sid: $('input[name=sid]').val(),
      confirm: 'Yes',
    })
    this.confirmActionSuccess(response, M.POST_DELETED)
  },

  // Gets and transforms the posts in a thread page.
  async _getPosts(threadUrl) {
    let {body: threadPage} = await threadUrl.get()
    let $ = cheerio.load(threadPage)

    return $('.post').map(function() {
      let $entry = $(this)
      let subject = $entry.find('h3').text()
      let id = $entry.attr('id').replace(/^p/, '')
      let authorLine = $entry.find('.author').text()
      let [, author, date] = /by\s+(.+?)\s*»\s*(.+?)\s*$/.exec(authorLine)
      let message = $.html($entry.find('.content'), {xmlMode: true, decodeEntities: false})

      return {
        id,
        subject,
        author,
        date,
        message,
      }
    })
    .get()
  },

  // Get a post.
  async getPost(postId) {
    let posts = await this._getPosts(this.rel`viewtopic.php?p=${postId}`)
    return posts.filter(({id}) => id === `${postId}`)[0]
  },

  // Lists the posts in a thread page.
  async listPosts(forumId, threadId, start=0) {
    let posts = await this._getPosts(this.rel`viewtopic.php?f=${forumId}&t=${threadId}&start=${start}`)
    for (let post of posts) delete post.message
    return posts
  },
}
