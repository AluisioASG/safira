import * as M from './util/messages'
import {initBBcodeFormResponse} from './util/form-response'


export default {
  // Sets the user's signature.
  async setSignature(newText, options={}) {
    const formUrl = this.rel`ucp.php?i=profile&mode=signature`
    // Get the form validation entries.
    let {body: formBody} = await formUrl.get()
    // Now fill the response form and send it.
    let {form: formData} = initBBcodeFormResponse(formBody, options)
    Object.assign(formData, {signature: newText})
    let postResponse = await formUrl.post(formData)
    this.confirmActionSuccess(postResponse, M.PROFILE_UPDATED)
  },
}
