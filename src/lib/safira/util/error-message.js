import cheerio from 'cheerio'


export default function getMessage($body) {
  if (typeof $body === 'string') {
    $body = cheerio.load($body)
  }

  // The message we're interested in is contained in the first child,
  // a text node, for "Information" pages.  In the login screen, it's
  // elsewhere, and there we want to remove the "please check" bits.
  return $body('#message p').contents().eq(0).text().trim() ||
         $body('.error').text().replace(/Please check your .+$/, '').trim() ||
         null
}
