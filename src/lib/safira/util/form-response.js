import cheerio from 'cheerio'

import {POSTING_OPTIONS_SCHEMA} from '../../config'


// Fills common form fields used by phpBB to validate forms.  The value
// of these fields is taken from a previous caller-issued request to
// the form's page.
// For convenience, the `cheerio` wrapper for the form page is returned
// with the form data, in case the caller needs to pull more data from
// the page.
export function initFormResponse(formPageBody) {
  let $ = cheerio.load(formPageBody)

  let form = {
    form_token: $('[name=form_token]').val(),
    creation_time: $('[name=creation_time]').val(),
    // For messages, it's `post`.  For UCP, `submit`.  Including both
    // won't hurt, right?
    post: 'Submit',
    submit: 'Submit',
  }
  return {$, form}
}

// Like `initFormResponse`, but also processes BBcode parsing options.
export function initBBcodeFormResponse(formPageBody, parsingOptions={}) {
  let {$, form} = initFormResponse(formPageBody)
  for (let opt of Object.keys(POSTING_OPTIONS_SCHEMA.properties)) {
    if (parsingOptions[opt]) form[opt] = 'on'
  }
  return {$, form}
}
