// phpBB messages follow.  In the future, we may make this configurable
// so different languages are supported.

// ucp.php
export const LOGIN_REDIRECT  = 'You have been successfully logged in.'
export const LOGOUT_REDIRECT = 'You have been successfully logged out.'
export const MESSAGE_STORED  = 'This message has been sent successfully.'
export const PROFILE_UPDATED = 'Your profile has been updated.'
// posting.php
export const POST_STORED     = 'This message has been posted successfully.'
export const POST_STORED_MOD = 'This message has been submitted successfully, but it will need to be approved by a moderator before it is publicly viewable.'
export const POST_EDITED     = 'This message has been edited successfully.'
export const POST_EDITED_MOD = 'This message has been edited successfully, but it will need to be approved by a moderator before it is publicly viewable.'
export const POST_DELETED    = 'This message has been deleted successfully.'
export const MESSAGE_DELETED = 'This message has been deleted successfully.'
