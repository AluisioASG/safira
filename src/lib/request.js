import url from 'url'

import request from 'request'

import {default as E, ERROR_PROPERTIES as EP} from './quasis/error'
import {loadFromPackageRoot} from './package-root'


// Default options for the `request` module.
const REQUEST_DEFAULTS = Object.freeze((() => {
  let pkg = loadFromPackageRoot('package.json')
  let ua = `${pkg.name}/${pkg.version} (+${pkg.homepage}) node/${process.version}`
  return {
    gzip: true,
    headers: {
      'User-Agent': ua,
    },
  }
})())


// Issues a `request` and returns a Promise for its completion.
function promiseRequest(options) {
  return new Promise((resolve, reject) => {
    request(options, (err, res) => err ? reject(err) : resolve(res))
  })
}


// A wrapper around the `request` module to make it more readily
// consumable by our codebase.  It applies our own default options
// (see `REQUEST_DEFAULTS` above), asserts a successful status code
// for the response (in the 2xx range) and returns a Promise.
export default async function request(options) {
  // Parse the target URL so we can use it later in case of error.
  let target = options.uri || options.url
  if (typeof target === 'string') target = url.parse(target)
  // Build the options object.
  options = Object.assign({}, REQUEST_DEFAULTS, options, {uri: target, url: undefined})
  let resp = await promiseRequest(options)
  // Reject unsuccesful status codes.
  // If we're handling redirects, 3xx codes are not expected; but if we
  // aren't, we should just pass them along to the caller.
  let handledRedirects = resp.followRedirects || resp.followAllRedirects
  if (resp.statusCode < 200 || resp.statusCode >= (handledRedirects ? 300 : 400)) {
    let lastUrl = resp.request.uri
    throw E`
      "${lastUrl.href}" ${resp.statusCode} ${resp.statusMessage}
      ${EP} HttpError ${target}:originalUrl ${lastUrl}:url ${resp}:response
    `
  }
  // Make it easier to get the final URL requested.
  resp.url = resp.request.uri
  return resp
}

// Re-export the cookie jar creator.
export {jar as cookieJar} from 'request'
