import fs from 'fs'
import path from 'path'


export default function findPackageRoot(at=__dirname) {
  if (fs.existsSync(path.join(at, 'package.json'))) {
    return at
  } else {
    const nextDir = path.dirname(at)
    return nextDir === at ? null : findPackageRoot(nextDir)
  }
}

export function fromPackageRoot(filename) {
  return path.join(findPackageRoot(), filename)
}

export function loadFromPackageRoot(filename) {
  return require(fromPackageRoot(filename))
}
