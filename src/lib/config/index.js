import fs from 'fs'
import path from 'path'

import toml from 'toml'
import tv4 from 'tv4'

import {fromPackageRoot, loadFromPackageRoot} from '../package-root'
import {default as E, ERROR_PROPERTIES as EP} from '../quasis/error'


// The JSON Schema config files are expected to match.
export const CONFIG_SCHEMA = loadFromPackageRoot('config-schemas/schema.json')
// JSON Schema for phpBB posting options.
export const POSTING_OPTIONS_SCHEMA = loadFromPackageRoot('config-schemas/posting_options.json')
// JSON Schema for XSLT-based message transforms.
export const MESSAGE_TRANSFORMS_SCHEMA = {
  $schema: 'http://json-schema.org/schema#',
  enum: ['none'].concat([for (f of fs.readdirSync(fromPackageRoot('post-xslt')))
                         path.basename(f, '.xsl')]),
}

// Get and set up our own validator instance.
const validator = tv4.freshApi()
validator.addSchema('posting_options.json', POSTING_OPTIONS_SCHEMA)
validator.addSchema('message_transforms', MESSAGE_TRANSFORMS_SCHEMA)

// Get the path to the config file.
function getConfigFilePath() {
  return process.env.SAFIRA_CONFIG || fromPackageRoot('.config.toml')
}

// Read and parse the given config file.
function parseConfigFile(filename) {
  let text = fs.readFileSync(filename, 'utf8')
  try {
    return toml.parse(text)
  } catch (ex) {
    throw E`
      ${filename}:${ex.line}:${ex.column}: ${ex.message}
      ${EP} MalformedConfigFile ${ex}:cause
    `
  }
}


export function validateFromFile(filename, obj, schema) {
  // Report all errors found, don't try to handle recursive structures,
  // and consider unspecified properties errors.
  let result = validator.validateMultiple(obj, schema, false, true)
  if (result.valid) return
  let message = result.errors
    .map(error => `  At config item ${error.dataPath || '/'}: ${error.message}`)
    .join('\n')
  throw E`
    ${filename}\n${message}
    ${EP} InvalidConfigFile ${result.errors}:cause
  `
}

export function loadConfig() {
  let filename = getConfigFilePath()
  let config = parseConfigFile(filename)
  validateFromFile(filename, config, CONFIG_SCHEMA)
  return config
}
