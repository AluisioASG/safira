import glob from 'glob'
import Mocha from 'mocha'
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import nock from 'nock'


// Get us a new mocha.
const mocha = new Mocha({
  ui: 'tdd',
  reporter: process.env.MOCHA_REPORTER || 'landing',
  timeout: 2150, // 2 POSTs (see 40bd2d9) + processing
  slow: 2000, // 2 POSTS is red, 1 POST is yellow
})
// And a chaining, eventful assertion.
chai.use(chaiAsPromised)

// Add test folders to the suite.  Other source files are to be support
// modules for the tests.
glob.sync('test/tests/*.js', {nodir: true})
.forEach(mocha.addFile, mocha)

// We don't want to send any packets during our test run.
nock.disableNetConnect()

// Run the tests and indicate the number of failures in the exit code.
mocha.run(failures => {
  process.on('exit', () => process.exit(failures))
})
