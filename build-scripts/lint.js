import fs from 'fs'

import glob from 'glob'
import recast from 'recast'
import {JSHINT as jshint} from 'jshint'
import {reporter} from 'jshint-stylish/stylish'


// Dumb down our ES6 code so JSHint has a chance at parsing it.
// Put the visitor object outside the preprocessing function so we
// don't recreate it for every file.
const astVisitor = {
  // jshint/jshint#1939, async/await.  Convert into generator.
  visitFunction(path) {
    const node = path.node
    if (node.async) {
      // Generator arrows are not defined.
      if (node.type === 'ArrowFunctionExpression') {
        node.type = 'FunctionExpression'
      }
      node.async = false
      node.generator = true
    }
    this.traverse(path)
  },
  visitAwaitExpression(path) {
    path.node.type = 'YieldExpression'
    this.traverse(path)
  },
}
function preprocess(code) {
  // recast has a hard time parsing #!s.  Also, our input is a Buffer.
  code = code.toString().replace(/^#!/, '//$&')
  // Oh, the irony.  recast doesn't support exponentiation.
  code = code.replace(/\b(-?\d+)\*\*(-?\d+)\b/g, ($0, a, b) => Math.pow(+a, +b))
  const origAst = recast.parse(code)
  const newAst = recast.visit(origAst, astVisitor)
  return recast.print(newAst).code
}

// Make the linter an object so we can store inter-file state.
const Linter = {
  config: {},
  results: [],
  data: [],

  lint(file, code) {
    if (!jshint(code, this.config)) {
      this.results.push(...[for (error of jshint.errors) {file, error}])
      this.data.push(Object.assign(jshint.data(), {file}))
    }
  },
  report(reporter) {
    reporter(this.results, this.data)
  },
}


// Load JSHint's config, lint the source code and the build scripts,
// then report the results.
Linter.config = require('../package.json').jshintConfig
glob.sync('{src/**,build-scripts}/*.js', {nodir: true})
.forEach(filename => Linter.lint(filename, preprocess(fs.readFileSync(filename))))
Linter.report(reporter)
