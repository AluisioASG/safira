# safira
_the static automated forum-interfacing relay assistant_


safira is a command-line utility for interacting with phpBB-based forums.  It allows you to:

- reply to existing topics
- check, read and reply to private messages
- change your signature

with more features on the way.


## Installing and configuring

safira is a [Node.js] application, but is not available through npm (a common pattern for its author).  Instead, you'll need to get the source code from https://gitlab.com/AluisioASG/safira, install the dependencies, build and link it to the global install prefix:

```
git clone https://gitlab.com/AluisioASG/safira.git
cd safira
npm install
npm link
```

(Yes, we know this process is cucumbersome.  It won't be long before we fix it, I promise.)

To configure safira, create a file `.config.toml` at the root of the source code tree.  Alternatively, you can create it anywhere and then point safira to it by way of the `SAFIRA_CONFIG` environment variable.  This latter method will also support installs made via npm in addition to clones of the source code repository.

The config file must be a valid [TOML] document with the following format:

```toml
forum = "http://myapp.example.com/path/to/forum/no-last-slash"

[credentials]
username = "MeMyselfAndI"
password = "ultr4!hAkkeR-(mathy_thing)p**DWORD"

[posting_options]
attach_sig = true

[favorite_threads]
1812 = "f2t10495"
guarany = "f7t101043"
```

`posting_options` lets you select the default options applied to replies and messages: `disable_bbcode`, `disable_smilies` and `attach_sig` are self-explanatory, `disable_magic_url` disables automatic URL linking and `notify` asks the forum software to notify you when a reply is posted.

`favorite_threads` allows you to alias topics you interact with frequently, so you don't need to remember the forum and topic IDs.

The two above are optional config items; everything else is required ― or safira may end up doing something you didn't mean to.


## Using

### Input conventions

Wherever you need to **specify a forum topic**, use the format <code>f<var>[forumId]</var>t<var>[topicId]</var></code> or an alias (see `favorite_threads` above).  The value of `forumId` and `topicId` are normally visible in the URL for any of the topic's pages, and always in the link in the topic title below the site header.  For example, a topic with an URL ending in `/viewtopic.php?f=31&t=101051&start=20800` is mapped to `f31t101051`.

Commands that send **post-like messages** (i.e. `reply` and `pm-send`) have an unified input format: an optional JSON document corresponding to the `posting_options` setting in the first line, then the message subject, and finally the content.  These three items may have any number of blank lines before each of them.  All the examples below are valid:

```
This is the subject
This can be either a new topic, a reply to an existing topic, or a private message.

We could have added a blank line after the subject, as we'll do in the next example, but it doesn't matter.  The one before this paragraph does, though, and is sent to the forum as-is.
```

```

{"attach_sig": false, "disable_bbcode": true}

An example with no signature and no BBCode

Note that [b]options[/b] (and that won't be made [/b]bold[b]) are all unset by default; that's why `attach_sig` is set in the config file: so we don't have to specify it with every message.
```

### Through the command line

Well, that's simple: <kbd>safira <var>command</var> <var>args…</var></kbd>.  Run <kbd>safira --help</kbd> for the available commands and their parameters.  Any input that wouldn't fit in the command line (e.g. posts and messages) is taken from the standard input.

### Through the API

TBA


## Coding and contributing

### Source tree organization

First and foremost, there is `lib`.  That's safira's API proper.  In there lies the underlying session keeping and high-level HTTP request processing code, as well as functions for execution of specific tasks.

Then there's `bin` and `config`, which compose the command-line interface.  The latter handles parsing and validation of the executable's config file, while the former does all the command-line and input parsing (when a command needs a post-like input), command delegation and execution.  These commands are located at `bin/commands` and digest the user input for consumption of the API and pretty-print the result back to the user.

You may be asking yourself why `bin` and `config` are separated, and the latter not a subdirectory of the former.  That's because `config` includes the list of parsing options for posts that phpBB accepts, and that list is used by the API.

Finally, there's `util`, a collection of modules that may be useful outside of our problem domain.  Micro-libraries, so to speak.


### Code quality

While the codebase ought to be checked with [JSHint] and [JSCS], it currently is not due to incomplete support from these tools for some of the ES.next features we use.

### Licensing

All code in safira's source code repository is, unless explicitly stated otherwise, licensed under the Mozilla Public License, version 2.0.  By contributing code to the project, you agree to the distribution of your work under that same license and certify you have the rights to do so.


[TOML]: https://github.com/toml-lang/toml
[Node.js]: https://nodejs.org/
[JSHint]: http://jshint.com/
[JSCS]: https://github.com/jscs-dev/node-jscs
